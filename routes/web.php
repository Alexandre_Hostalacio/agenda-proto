<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/email', function () {
    return new WelcomeMail();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contacts', 'ContactController@index');

Route::get('/contacts/create', 'ContactController@create');

Route::post('/contacts', 'ContactController@store');

//Laravel automatically resolves Eloquent models defined
//in routes or controller actions whose type-hinted variable
//names match a route segment name

Route::get('/contacts/{contact}', 'ContactController@show');

Route::get('/contacts/{contact}/edit', 'ContactController@edit');

Route::patch('/contacts/{contact}', 'ContactController@update');

Route::delete('/contacts/{contact}', 'ContactController@destroy');

Route::get('/contacts/export/pdf', 'ContactsExportController@exportPDF');

Route::get('/contacts/export/xlsx', 'ContactsExportController@exportXLSX');

Route::get('/import/contacts', 'PeopleAPIController@import');

Route::get('/import', 'PeopleAPIController@importCode');



//Route::get('/contacts/export', ['as' => 'contacts-export', 'uses' => 'ContactsExportController@export']);
