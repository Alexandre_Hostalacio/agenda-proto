<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->group(function () {
    Route::get('/teste', function () {
        return ['teste'];
    });

    Route::get('/id', function () {
        return auth()->user()->id;
    });

    Route::get('/contacts', 'ApiContactController@index');

    Route::get('/contacts/{contact}', 'ApiContactController@show');

    Route::patch('/contacts/{contact}', 'ApiContactController@update');
});


// Route::group(['middleware' => 'auth:api'], function () {
//     //Route::get('/contacts', 'ApiContactController@index');
//     Route::get('/contacts', function () {
//         return ['teste' => 'teste'];
//     });
// });
