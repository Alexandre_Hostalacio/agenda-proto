let phoneIndex = Number(document.getElementById("GetPhoneLength").innerHTML) + 1;
if (phoneIndex == 1) phoneIndex = 2;
console.log(phoneIndex);
let addressIndex = Number(document.getElementById("GetAddressLength").innerHTML) + 1;
if (addressIndex == 1) addressIndex = 2;

let nada = "";

buscaCepOnButton(nada);

for (let i = 2; i < addressIndex; i++) {
    buscaCepOnButton(i);
}

document.getElementById('addPhoneBtn').onclick = function () {
    let template = `<br>
    <label for="newPhoneInp${phoneIndex}"><i class="fas fa-phone-alt fa-lg mr-3"></i>Fone ${phoneIndex}</label>
    </div>
    <input type="text" name="phones[${phoneIndex}]" class="form-control"
    id="newPhoneInp${phoneIndex}" placeholder="(99)9999-9999" oninput="phoneMask(this)">
    `;
    let container = document.getElementById('divPhone');
    let div = document.createElement('div');
    div.innerHTML = template;
    container.appendChild(div);
    phoneIndex++;
}

document.getElementById('addAddressBtn').onclick = function () {
    let template = `<br>
    <label for="newAddressInp${addressIndex}"><strong>Endereço ${addressIndex}</strong></label>
    <br>
    <button type="button" class="btn btn-dark m-3" id="btnBuscaCEP${addressIndex}">Buscar pelo CEP</button>
    <br>
    <div class="form-row">
        <div class="form-group col-md-2">
            <label for="newCEPInp${addressIndex}">CEP</label>
            <input type="text" name="addresses[${addressIndex}][cep]" class="form-control cepInp" id="newCEPInp${addressIndex}"
                placeholder="CEP">
        </div>
        <div class="form-group col-md-8">
            <label for="newStreetInp${addressIndex}">Logradouro</label>
            <input type="text" name="addresses[${addressIndex}][street]" class="form-control" id="newStreetInp${addressIndex}"
                placeholder="Rua">
        </div>
        <div class="form-group col-md-2">
            <label for="newHouseNumberInp${addressIndex}">Número</label>
            <input type="number" name="addresses[${addressIndex}][number]" class="form-control" id="newHouseNumberInp${addressIndex}"
                placeholder="Número">
        </div>
    </div>

    <br>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="newCompleInp${addressIndex}">Complemento</label>
            <input type="text" name="addresses[${addressIndex}][complement]" class="form-control" id="newCompleInp${addressIndex}"
                placeholder="Complemento">
        </div>
        <div class="form-group col-md-6">
            <label for="newDistrictInp${addressIndex}">Bairro</label>
            <input type="text" name="addresses[${addressIndex}][district]" class="form-control" id="newDistrictInp${addressIndex}"
                placeholder="Bairro">
        </div>
    </div>

    <br>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="newDistricttInp${addressIndex}">Cidade</label>
            <input type="text" name="addresses[${addressIndex}][city]" class="form-control" id="newCityInp${addressIndex}"
                placeholder="Cidade">
        </div>
        <div class="form-group col-md-6">
            <label for="newDistricttInp${addressIndex}">Estado</label>
            <input type="text" name="addresses[${addressIndex}][state]" class="form-control" id="newStateInp${addressIndex}"
                placeholder="Estado">
        </div>
    </div>
    `;
    let container = document.getElementById('divAddress');
    let div = document.createElement('div');
    div.innerHTML = template;
    container.appendChild(div);
    buscaCepOnButton(addressIndex);
    addressIndex++;
}

function buscaCepOnButton(count) {
    $(`#btnBuscaCEP${count}`).on('click', function () {
        let cep = $(`#newCEPInp${count}`).val();
        cep = cep.replace(/\D/g, '');
        var validaCEP = /^[0-9]{8}$/; // Regular Expression
        if (validaCEP.test(cep)) {
            $.ajax({
                url: `https://viacep.com.br/ws/${cep}/json`,//Usando viacep.com.br para buscar endereço pelo CEP
                success: function (conteudo) {
                    console.log(conteudo);
                    if (!("erro" in conteudo)) {
                        //Atualiza os campos com os valores.
                        $(`#newStreetInp${count}`).val(conteudo.logradouro);
                        $(`#newCompleInp${count}`).val(conteudo.complemento);
                        $(`#newDistrictInp${count}`).val(conteudo.bairro);
                        $(`#newCityInp${count}`).val(conteudo.localidade);
                        $(`#newStateInp${count}`).val(conteudo.uf);
                    } //end if.
                    else {
                        //CEP não Encontrado.
                        alert("CEP não encontrado.");
                        $(`#newCEPInp${count}`).val('');
                    }
                },
            });
        } else { alert("CEP Invalido") }
    });
}

function phoneMask(phoneInput) {
    var phone = phoneInput.value;
    var phone = phone.replace(/\D/g, '');
    var reex = phone.length == 11 ? /(\w{2})(\w{5})(\w{4})/ : /(\w{2})(\w{4})(\w{4})/;
    var n = 11 - phone.length;
    if (n > 0) phone = phone.concat((new Array(n).fill('_')).join(''));
    var x = phone.match(reex);
    phoneInput.value = '(' + x[1] + ') ' + x[2] + '-' + x[3];
}
//Fazer mascara para o cep
function cepMask(cepInput) {
    var cep = cepInput.value;
    var cep = cep.replace(/\D/g, '');
    //var reex = cep.length == 11 ? /(\w{2})(\w{5})(\w{4})/ : /(\w{2})(\w{4})(\w{4})/;
    var reex =  /(\w{2})(\w{3})(\w{3})/;
    var n = 11 - cep.length;
    if (n > 0) phone = phone.concat((new Array(n).fill('_')).join(''));
    var x = phone.match(reex);
    cepInput.value = x[1] + '.' + x[2] + '-' + x[3];
}