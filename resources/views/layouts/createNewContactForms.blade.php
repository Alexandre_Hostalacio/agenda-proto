@csrf
<label for="newNameInp">Nome</label>
<input type="text" name="name" class="form-control" id="newNameInp" placeholder="Nome" required
    value="{{old('name') ?? $contact->name}}">
@error('name')
<small class="text-danger">{{$message}}</small>
@enderror
<br>
<label for="newEmailInp"><i class="fas fa-at fa-lg mr-1"></i>Email</label>
<input type="email" name="email" class="form-control" id="newEmailInp" placeholder="Email" required
    value="{{old('email') ?? $contact->email}}">
@error('email')
<small class="text-danger">{{$message}}</small>
@enderror
<br>
<label for="inlineFormCustomSelect">Categorias</label>
<select name="categories" class="form-control custom-select" id="inlineFormCustomSelect" multiple>
    <option value="" selected></option>
    <option value="família">Família</option>
    <option value="trabalho">Trabalho</option>
    <option value="amigos">Amigos</option>
</select>
@error('categories')
<small class="text-danger">{{$message}}</small>
@enderror
<br>
<br>
<div id="GetPhoneLength" style="display:none;">{{count($contact->phones??[])}}</div>
<div id="divPhone">
    <div>
        <label for="newPhoneInp"><i class="fas fa-phone-alt fa-lg mr-3"></i>Fone 1</label>
        <div class="btn-group btn-group-toggle">
            <a class="btn btn-dark btn-sm ml-4" id="addPhoneBtn">+</a>
            {{-- <button class="btn btn-dark btn-sm ml-4" id="rmvPhoneBtn">-</button> --}}
        </div>
        <input type="text" name="phones[1]" class="form-control" id="newPhoneInp" placeholder="(99)9999-9999"
            value="{{old('phones')['1'] ?? $contact->phones['1']}}" oninput="phoneMask(this)">
    </div>
    {{-- For loop to get in the Forms all Phones on Contact --}}
    @forelse ($contact->phones??[] as $key => $phone)
    @if ($key != 1)
    <br>
    <div>
        <label for="newPhoneInp{{{$key}}}"><i class="fas fa-phone-alt fa-lg mr-3"></i>Fone {{{$key}}}</label>
        <input type="text" name="phones[{{{$key}}}]" class="form-control" id="newPhoneInp{{{$key}}}"
            placeholder="(99)9999-9999" value="{{$phone}}" oninput="phoneMask(this)">
    </div>
    @endif
    @empty
    @endforelse
</div>
<br>
@error('phone')
<small class="text-danger">{{$message}}</small>
@enderror
<div id="GetAddressLength" style="display:none;">{{count($contact->addresses??[])}}</div>
<div id="divAddress">
    <div>
        {{-- ##### Address ##### --}}
        <label for="newAddressInp"><strong>Endereço 1</strong></label>
        <div class="btn-group btn-group-toggle">
            <a class="btn btn-dark btn-sm ml-4" id="addAddressBtn">+</a>
            {{-- <button class="btn btn-dark btn-sm ml-4" id="rmvAddressBtn">-</button> --}}
        </div>
        <br>
        <button type="button" class="btn btn-dark m-3" id="btnBuscaCEP">Buscar pelo CEP</button>
        <br>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="newCEPInp">CEP</label>
                <input type="text" name="addresses[1][cep]" class="form-control cepInp" id="newCEPInp"
                    placeholder="CEP" value="{{old('addresses')['1']['cep'] ?? $contact->addresses['1']['cep']}}">
            </div>
            <div class="form-group col-md-8">
                <label for="newStreetInp">Logradouro</label>
                <input type="text" name="addresses[1][street]" class="form-control" id="newStreetInp"
                    placeholder="Rua" value="{{old('addresses')['1']['street'] ?? $contact->addresses['1']['street']}}">
            </div>
            <div class="form-group col-md-2">
                <label for="newHouseNumberInp">Número</label>
                <input type="number" name="addresses[1][number]" class="form-control" id="newHouseNumberInp"
                    placeholder="Número"
                    value="{{old('addresses')['1']['number'] ?? $contact->addresses['1']['number']}}">
            </div>
        </div>

        <br>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="newCompleInp">Complemento</label>
                <input type="text" name="addresses[1][complement]" class="form-control" id="newCompleInp"
                    placeholder="Complemento"
                    value="{{old('addresses')['1']['complement'] ?? $contact->addresses['1']['complement']}}">
            </div>
            <div class="form-group col-md-6">
                <label for="newDistrictInp">Bairro</label>
                <input type="text" name="addresses[1][district]" class="form-control" id="newDistrictInp"
                    placeholder="Bairro"
                    value="{{old('addresses')['1']['district'] ?? $contact->addresses['1']['district']}}">
            </div>
        </div>

        <br>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="newDistricttInp">Cidade</label>
                <input type="text" name="addresses[1][city]" class="form-control" id="newCityInp"
                    placeholder="Cidade" value="{{old('addresses')['1']['city'] ?? $contact->addresses['1']['city']}}">
            </div>
            <div class="form-group col-md-6">
                <label for="newDistricttInp">Estado</label>
                <input type="text" name="addresses[1][state]" class="form-control" id="newStateInp"
                    placeholder="Estado"
                    value="{{ old('addresses')['1']['state'] ?? $contact->addresses['1']['state']}}">
            </div>
        </div>
        {{-- ##### Address ##### --}}
    </div>
    @forelse ($contact->addresses??[] as $key => $address)
    @if ($key != 1)
    <div>
        {{-- ##### Address ##### --}}
        <label for="newAddressInp{{$key}}"><strong>Endereço {{$key}}</strong></label>
        <br>
        <button type="button" class="btn btn-dark m-3" id="btnBuscaCEP{{$key}}">Buscar pelo CEP</button>
        <br>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="newCEPInp{{$key}}">CEP</label>
                <input type="text" name="addresses[{{$key}}][cep]" class="form-control cepInp" id="newCEPInp{{$key}}"
                    placeholder="CEP" value="{{old('addresses')[$key]['cep'] ?? $contact->addresses[$key]['cep']}}">
            </div>
            <div class="form-group col-md-8">
                <label for="newStreetInp{{$key}}">Logradouro</label>
                <input type="text" name="addresses[{{$key}}][street]" class="form-control" id="newStreetInp{{$key}}"
                    placeholder="Rua"
                    value="{{old('addresses')[$key]['street'] ?? $contact->addresses[$key]['street']}}">
            </div>
            <div class="form-group col-md-2">
                <label for="newHouseNumberInp{{$key}}">Número</label>
                <input type="number" name="addresses[{{$key}}][number]" class="form-control"
                    id="newHouseNumberInp{{$key}}" placeholder="Número"
                    value="{{old('addresses')[$key]['number'] ?? $contact->addresses[$key]['number']}}">
            </div>
        </div>

        <br>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="newCompleInp{{$key}}">Complemento</label>
                <input type="text" name="addresses[{{$key}}][complement]" class="form-control" id="newCompleInp{{$key}}"
                    placeholder="Complemento"
                    value="{{old('addresses')[$key]['complement'] ?? $contact->addresses[$key]['complement']}}">
            </div>
            <div class="form-group col-md-6">
                <label for="newDistrictInp{{$key}}">Bairro</label>
                <input type="text" name="addresses[{{$key}}][district]" class="form-control" id="newDistrictInp{{$key}}"
                    placeholder="Bairro"
                    value="{{old('addresses')[$key]['district'] ?? $contact->addresses[$key]['district']}}">
            </div>
        </div>

        <br>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="newDistricttInp{{$key}}">Cidade</label>
                <input type="text" name="addresses[{{$key}}][city]" class="form-control" id="newCityInp{{$key}}"
                    placeholder="Cidade" value="{{old('addresses')[$key]['city'] ?? $contact->addresses[$key]['city']}}">
            </div>
            <div class="form-group col-md-6">
                <label for="newDistricttInp{{$key}}">Estado</label>
                <input type="text" name="addresses[{{$key}}][state]" class="form-control" id="newStateInp{{$key}}"
                    placeholder="Estado"
                    value="{{old('addresses')[$key]['state'] ?? $contact->addresses[$key]['state']}}">
            </div>
        </div>
        {{-- ##### Address ##### --}}
    </div>
    @endif
    @empty
    @endforelse
</div>
<br>
{{-- teste --}}
{{-- <label for="teste">Endereço</label>
                    <input type="text" name="address" class="form-control" id="teste" placeholder="Endereço"> --}}
{{-- teste --}}
@error('address')
<small class="text-danger">{{$message}}</small>
@enderror
<br>
<button type="submit" class="form-control btn btn-dark" id="btnSalvar">Salvar</button>