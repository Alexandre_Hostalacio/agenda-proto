<div class="contactForm card col-lg-12">
    <div class="card-body">
        <form id="newContactForm" action="/contacts" method="post" enctype="multipart/form-data">
            @csrf
            <label for="newNameInp">Nome</label>
            <input type="text" name="name" class="form-control" id="newNameInp" placeholder="Nome" required>
            @error('name')
                <small class="text-danger">{{$message}}</small>
            @enderror
            <br>
            <label for="newEmailInp"><i class="fas fa-at fa-lg mr-1"></i>Email</label>
            <input type="email" name="email" class="form-control" id="newEmailInp" placeholder="Email">
            @error('email')
                <small class="text-danger">{{$message}}</small>
            @enderror
            <br>
            <label for="inlineFormCustomSelect">Categorias</label>
            <select name="categories" class="form-control custom-select" id="inlineFormCustomSelect" multiple>
                <option value="" selected></option>
                <option value="família">Família</option>
                <option value="trabalho">Trabalho</option>
                <option value="amigos">Amigos</option>
            </select>
            @error('categories')
                <small class="text-danger">{{$message}}</small>
            @enderror
            <br>
            <br>
            <label for="newPhoneInp"><i class="fas fa-phone-alt fa-lg mr-3"></i>Fone</label>
            <div class="btn-group btn-group-toggle">
                <button class="btn btn-dark btn-sm ml-4" id="addPhoneBtn">+</button>
                <button class="btn btn-dark btn-sm ml-4" id="rmvPhoneBtn">-</button>
            </div>
            <input type="number" name="phone" class="form-control" id="newPhoneInp" placeholder="Phone">
            <div id="divPhone">
            </div>
            <br>
            @error('phone')
                <small class="text-danger">{{$message}}</small>
            @enderror
            <label for="newAddressInp"><strong>Endereço 1</strong></label>
            <div class="btn-group btn-group-toggle">
                <button class="btn btn-dark btn-sm ml-4" id="addAddressBtn">+</button>
                <button class="btn btn-dark btn-sm ml-4" id="rmvAddressBtn">-</button>
            </div>
            <br>
            <div id="divAddress">
            </div>
            <br>
            {{-- teste --}}
            <label for="teste">Endereço</label>
            <input type="text" name="address" class="form-control" id="teste" placeholder="Endereço">
            {{-- teste --}}
            @error('address')
                <small class="text-danger">{{$message}}</small>
            @enderror
            <br>
            <button type="submit" class="form-control btn btn-dark" id="btnSalvar">Salvar</button>
        </form>
    </div>
</div>
</div>`