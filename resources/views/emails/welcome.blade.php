@component('mail::message')
# Usuário cadastrado com sucesso

Bem vindo ao app Agenda

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
