@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar cadastro</div>
                {{-- forms to create new contact --}}
                <div class="contactForm card col-lg-12">
                    <div class="card-body">
                        <form id="newContactForm" action="/contacts/{{$contact->id}}" method="post" enctype="multipart/form-data">
                            @method('patch')

                            @include('layouts.createNewContactForms')

                        </form>
                    </div>
                </div>
                {{-- forms-end --}}
            </div>
        </div>
    </div>
</div>
@endsection