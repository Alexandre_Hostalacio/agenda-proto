@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header mb-3 lead"><a href="/contacts/{{$contact->id}}/edit"><strong>{{$contact->name}}
                            - Edit</strong></a>
                </div>
                <p class="card-text pl-3 pb-1 lead"><strong>e-mail:</strong> {{$contact->email}}</p>
                @foreach ($contact->phones??[] as $phone)
                <p class="card-text pl-3 pb-2 lead"><strong>phone:</strong> {{$phone}}</p>
                @endforeach
                @if ($contact->categories != null)
                <p class="card-text pl-3 pb-2 lead"><strong>categories:</strong> {{$contact->categories}} </p>
                @endif


                @foreach ($contact->addresses??[] as $address)
                <p class="card-text pl-3 pb-2 lead"><strong>address</strong> </p>
                @foreach ($address as $x => $x_value)
                <li class="list-group-item pl-3 pb-3 lead"> &nbsp;&nbsp;&nbsp;&nbsp; {{$x}}: {{$x_value}}</li>
                @endforeach
                @endforeach
                <br>
                <div class="row justify-content-center">
                    <form action="/contacts/{{$contact->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-dark">Delete</button>
                    </form>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection