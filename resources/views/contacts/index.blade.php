@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @forelse ($contacts as $contact)

            <div class="card">
                <div class="card-header mb-3 lead">
                    <strong>
                        <a href="/contacts/{{$contact->id}}">{{$contact->name}}</a>
                    </strong>
                </div>
                <div class="card-body">
                    <p class="card-text pl-3 pb-1 lead"><strong>email:</strong> {{$contact->email}}</p>
                    <p class="card-text pl-3 pb-3 lead"><strong>phone:</strong> {{$contact->phones[1]}}</p>
                </div>
            </div>

            <br>
            @empty
            <p>Sem Contatos</p>
            @endforelse
            {{-- {{dd(route('contacts-export'))}} --}}
            <a href="{{ url('/contacts/export/pdf') }}" class="btn btn-dark">Exportar PDF</a>
            <br>
            <br>
            <a href="{{ url('/contacts/export/pdf') }}" class="btn btn-dark">Exportar PDF Latex</a>
            <br>
            <br>
            <a href="{{ url('/contacts/export/xlsx') }}" class="btn btn-dark">Exportar Planilha xlsx</a>
        </div>
    </div>
</div>
@endsection