<table>
    <thead>
        <tr>
            <th align="center"><strong>Id</strong></th>
            <th align="center"><strong>Name</strong></th>
            <th align="center"><strong>E-mail</strong></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($contacts as $contact)
        <tr>
            <td align="center">{{$contact->id}}</td>
            <td align="center">{{$contact->name}}</td>
            <td align="center">{{$contact->email}}</td>
        </tr>
        @empty
        <tr>
            <td>Sem Contatos</td>
        </tr>
        @endforelse
    </tbody>
</table>