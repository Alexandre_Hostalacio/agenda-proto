<div class="container">
    <h3>Lista de Contatos</h3>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            @forelse ($contacts as $contact)

            <div class="card">
                <div class="card-header mb-3 lead">
                    <strong>
                        <a>{{$contact->name}}</a>
                    </strong>
                </div>
                <div class="card-body">
                    <p class="card-text pl-3 pb-1 lead"><strong>email:</strong> {{$contact->email}}</p>
                    <p class="card-text pl-3 pb-3 lead"><strong>phone:</strong> {{$contact->phones[1]}}</p>
                </div>
            </div>

            <br>
            @empty
            <p>Sem Contatos</p>
            @endforelse
        </div>
    </div>
</div>