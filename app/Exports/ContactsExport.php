<?php

namespace App\Exports;

use App\User;
use App\Contact;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;

//https://docs.laravel-excel.com/3.1/exports/collection.html
class ContactsExport implements
    //FromCollection,
    FromView,
    Responsable,
    ShouldAutoSize
//WithMapping
{
    use Exportable;

    /**
     *  var to get
     */
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    // public function collection()
    // {
    //     //trocar para usar a repository
    //     //dump($this->userId);
    //     //dd(Contact::where('user_id', $this->userId)->get());
    //     return Contact::where('user_id', $this->userId)->get();
    // }
    public function view(): View
    {
        return view('exports.templatePDF', [
            'contacts' => Contact::where(
                'user_id',
                $this->userId
            )->get()
        ]);
    }

    // public function map($contact): array
    // {
    //     return [
    //         $contact->id,
    //         $contact->name,
    //         $contact->email
    //     ];
    // }
}
