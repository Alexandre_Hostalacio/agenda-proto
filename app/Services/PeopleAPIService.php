<?php

namespace App\Services;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use App\Services\ContactService;
use App\Http\Requests\CreateContactRequest;
use Google\Client;
use Google_Service_PeopleService;
use App\Repositories\ContactRepository;
use phpDocumentor\Reflection\Types\Null_;

class PeopleAPIService
{
    /**
     * Repository of Contacts
     * @var ContactRepository
     */
    protected $contactRepository;

    public function __construct(
        ContactRepository $contactRepository
    ) {
        $this->contactRepository = $contactRepository;
    }
    /**
     * Get Contacts by User
     *
     * @param User $user
     *
     * @return $contacts
     */
    public function userContacts(User $user)
    {
        $contacts = $this->contactRepository->getContactsByUser($user);
        return $contacts;
    }
    /**
     * Create Contacts by User
     *
     * @param User $user
     *
     * @param Array $contacts
     */
    public function createContact(User $user, $contact)
    {
        $this->contactRepository->createContact($user, $contact);
    }
    /**
     * Update Contact
     *
     * @param Contact $contact
     *
     * @param Array $updatedContact
     */
    public function updateContact(Contact $contact, $updatedContact)
    {
        $this->contactRepository->update($updatedContact, $contact->id);
    }

    /**
     * get Client
     *
     * @return Client $client
     *
     */
    public function getClient()
    {
        $client = new Client();
        $client->setApplicationName('People API PHP Quickstart');
        $client->setScopes(Google_Service_PeopleService::CONTACTS_READONLY);
        //dd(asset('/home/alexandre/Projetos/agenda-proto/resources/credentials.json'));
        $client->setAuthConfig(storage_path() . '/credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        return $client;
    }
    /**
     * get Service using Auth Code
     *
     * @param string $authCode
     *
     * @return Google_Service_PeopleService $service
     *
     */
    public function getService($authCode)
    {
        $client = $this->getClient();

        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        $client->setAccessToken($accessToken);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
            throw new Exception(join(', ', $accessToken));
        }

        $service = new Google_Service_PeopleService($client);

        return $service;
    }
    /**
     * Save Imported Contacts on Data Base
     *
     * @param $Service_Results
     *
     */
    public function saveImpotedContacts($userId, $results)
    {
        if (count($results->getConnections()) == 0) {
            print "No connections found.\n";
            return;
        }
        foreach ($results->getConnections() as $person) {
            $contact = [];
            if (count($person->getNames()) != 0 && count($person->getEmailAddresses()) != 0) {
                $names = $person['names']; //$names = $person->getNames();
                $name = $names[0]['displayName'];
                //$name['displayName'];
                $emails = $person['emailAddresses'];
                $email = $emails[0]['value'];
                //$email['value'];
                $phone = null;
                if (count($person->getPhoneNumbers()) != 0) {
                    $phones = $person['phoneNumbers'];
                    $phone = $phones[0]['value'];
                    //$phone['value'];
                }
                $contact = [
                    'name' => $name,
                    'email' => $email,
                    'phones' => ['1' => $phone],
                ];

                //ver se tem contatos com mesmo email no banco de dados antes de criar um novo contato.

                $this->contactRepository->createContact($userId, $contact);
            }
        }
    }
}
