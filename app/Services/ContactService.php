<?php

namespace App\Services;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use App\Repositories\ContactRepository;

class ContactService
{
    /**
     * Repository of Contacts
     * @var ContactRepository
     */
    protected $contactRepository;

    public function __construct(
        ContactRepository $contactRepository
    ) {
        $this->contactRepository = $contactRepository;
    }
    /**
     * Get Contacts by User
     *
     * @param User $user
     *
     * @return $contacts
     */
    public function userContacts($userId)
    {
        $contacts = $this->contactRepository->getContactsByUserID($userId);
        //$contacts = $this->contactRepository->getContactsByUser($user);
        return $contacts;
    }
    /**
     * Create Contacts by User
     *
     * @param User $user
     *
     * @param Array $contacts
     */
    public function createContact($userId, $contact)
    {
        $this->contactRepository->createContact($userId, $contact);
        //$this->contactRepository->createContactbyUser($user, $contact);
    }
    /**
     * Confirm if user has contact
     *
     * @param User $user
     *
     * @param Array $contacts
     *
     * @return $contact if user has contact or false otherwise
     */
    public function hasUserContact($userId, $contact)
    {
        $contact = $userId == $contact->user_id ? $contact : false;
        return $contact;
    }
    /**
     * Update Contact
     *
     * @param Contact $contact
     *
     * @param Array $updatedContact
     */
    public function updateContact($contactId, $updatedContact)
    {
        if ($contactId) {
            //inverdter a ordem de $updatedContact e $contactId
            return $this->contactRepository->update($updatedContact, $contactId);
        }
        return false;
    }
    /**
     * Delete Contact
     *
     * @param Contact $contact
     */
    public function deleteContact($contactId)
    {
        if ($contactId) {
            $this->contactRepository->delete($contactId);
        }
    }
}
