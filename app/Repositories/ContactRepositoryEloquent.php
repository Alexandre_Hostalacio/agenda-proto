<?php

namespace App\Repositories;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ContactRepository;
//use App\Entities\Contact;
use App\Validators\ContactValidator;

/**
 * Class ContactRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ContactRepositoryEloquent extends BaseRepository implements ContactRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Contact::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /**
     * Get Contacts by User
     *
     * @param User $user
     *
     * @return $contacts
     */
    public function getContactsByUser(User $user)
    {
        $contacts = $user->contacts;
        return $contacts;
    }
    /**
     * Get Contacts by User Id
     *
     * @param $userId
     *
     * @return $contacts
     */
    public function getContactsByUserId($userId)
    {
        $contacts = Contact::where('user_id', $userId)->get();
        return $contacts;
    }
    /**
     * Create Contacts by User
     *
     * @param User $user
     *
     * @param Array $contacts
     */
    public function createContactByUser(User $user, $contact)
    {
        $user->contacts()->create($contact);
    }
    /**
     * Create Contacts by User Id
     *
     * @param User $user
     *
     * @param Array $contacts
     */
    public function createContact($userId, $contact)
    {
        $contact['user_id'] = $userId;
        Contact::create($contact);
    }
}
