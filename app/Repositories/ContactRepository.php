<?php

namespace App\Repositories;

use App\User;
use App\Contact;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ContactRepository.
 *
 * @package namespace App\Repositories;
 */
interface ContactRepository extends RepositoryInterface
{
    public function getContactsByUser(User $user);
    public function getContactsByUserId($userId);
    public function createContactByUser(User $user, $contact);
    public function createContact($userId, $contact);
}
