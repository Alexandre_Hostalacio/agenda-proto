<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Waavi\Sanitizer\Laravel\SanitizesInput;
use Illuminate\Foundation\Http\FormRequest;

class CreateContactRequest extends FormRequest
{
    use SanitizesInput;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }
    public function filters()
    {
        return [
            'name'  => 'trim|capitalize',
            'email' => 'trim|lowercase'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            //Estudar o comando optional para ver como ele funciona
            'email' => [
                'required',
                Rule::unique('contacts', 'email')
                    ->ignore(optional($this->route('contact'))->id)
                    ->where(function ($query) {
                        $query->where('user_id', auth()->user()->id);
                    })
            ],
            'categories' => 'nullable|string',
            'phones' => 'nullable|array|min:1', //numeric
            'phones.*' => 'nullable',
            'addresses' => 'nullable|array|min:1',
            'addresses.*' => 'nullable|array|min:7',
            'addresses.*.*' => 'nullable'
        ];
    }
}
