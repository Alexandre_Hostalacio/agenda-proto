<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use App\Services\ContactService;
use App\Http\Requests\CreateContactRequest;

class ContactController extends Controller
{
    /**
     * Service of Contacts
     * @var ContactService
     */
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->middleware('auth');
        $this->contactService = $contactService;
    }
    public function index()
    {
        //### passar o id no lugar do usuário ###
        $contacts = $this->contactService->userContacts(auth()->user()->id);
        return view('contacts.index', compact('contacts'));
    }
    public function create()
    {
        $contact = new Contact();
        return view('contacts.create', compact('contact'));
    }
    public function store(CreateContactRequest $request)
    {
        $contact = $request->validated();
        $this->contactService->createContact(auth()->user()->id, $contact);
        return redirect('/contacts');
    }
    public function show(Contact $contact)
    {
        //laravel fetch
        $contact = $this->contactService->hasUserContact(auth()->user()->id, $contact);
        if ($contact) {
            return view('contacts.show', compact('contact'));
        }
    }
    public function edit(Contact $contact)
    {
        $contact = $this->contactService->hasUserContact(auth()->user()->id, $contact);
        if ($contact) {
            return view('contacts.edit', compact('contact'));
        }
    }
    public function update(CreateContactRequest $request, Contact $contact)
    {
        $updatedContact = $request->validated();
        $contact = $this->contactService->hasUserContact(auth()->user()->id, $contact);
        $this->contactService->updateContact($contact->id ?? $contact, $updatedContact);
        return redirect('/contacts');
    }
    public function destroy(Contact $contact)
    {
        //usar o repository
        $contact = $this->contactService->hasUserContact(auth()->user()->id, $contact);
        if ($contact) {
            $this->contactService->deleteContact($contact->id);
        }
        return redirect('/contacts');
    }
}
