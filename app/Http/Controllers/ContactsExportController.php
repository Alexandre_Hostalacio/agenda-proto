<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use App\Services\ContactService;
use App\Exports\ContactsExport;
use Maatwebsite\Excel\Excel;

//use Maatwebsite\Excel\Facades\Excel;

class ContactsExportController extends Controller
{
    /**
     * Service of Contacts
     * @var Excel
     */
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->middleware('auth');
        $this->excel = $excel;
    }

    public function exportXLSX()
    {
        $sheetForm = new ContactsExport(auth()->user()->id);
        return $this->excel->download($sheetForm, 'contacts.xlsx');
    }

    public function exportPDF()
    {
        $sheetForm = new ContactsExport(auth()->user()->id);
        return $this->excel->download($sheetForm, 'contacts.pdf', Excel::DOMPDF);
    }
}
