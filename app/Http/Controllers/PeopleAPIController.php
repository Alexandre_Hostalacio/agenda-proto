<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
//use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Services\ContactService;
use App\Services\PeopleAPIService;
use App\Http\Requests\CreateContactRequest;
use Google\Client;
use Google_Service_PeopleService;

class PeopleAPIController extends Controller
{
    /**
     * Service of Contacts
     * @var ContactService
     */
    protected $contactService;
    /**
     * Service of Contacts
     * @var PeopleAPIService
     */
    protected $peopleService;

    public function __construct(ContactService $contactService, PeopleAPIService $peopleService)
    {
        $this->middleware('auth');
        $this->contactService = $contactService;
        $this->peopleService = $peopleService;
    }

    public function import()
    {
        $client = $this->peopleService->getClient();

        $tokenPath = 'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();

        return redirect($authUrl);
    }

    public function importCode(Request $request)
    {
        $authCode = $request->input('code');

        $service = $this->peopleService->getService($authCode);

        $optParams = array(
            'pageSize' => 1000,
            'resourceName' => 'people/me',
            'personFields' => 'names,emailAddresses,phoneNumbers',
        );
        $results = $service->people_connections->listPeopleConnections('people/me', $optParams);

        $this->peopleService->saveImpotedContacts(auth()->user()->id, $results);

        return redirect('/contacts');
    }
}
