<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use App\Services\ContactService;
use App\Http\Requests\CreateContactRequest;

class ApiContactController extends Controller
{
    /**
     * Service of Contacts
     * @var ContactService
     */
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->middleware('auth');
        $this->contactService = $contactService;
    }
    public function index()
    {
        //### passar o id no lugar do usuário ###
        $contacts = $this->contactService->userContacts(auth()->user()->id);
        //dd($contacts);
        return $contacts->all();
    }
    public function store(CreateContactRequest $request)
    {
        $contact = $request->validated();
        $this->contactService->createContact(auth()->user()->id, $contact);
        return [
            'status' => 'Contact was Stored',
            'contact' => $contact
        ];
    }
    public function show(Contact $contact)
    {
        //laravel fetch
        $this->contactService->createContact(auth()->user()->id, $contact);
        return $contact;
    }
    public function update(CreateContactRequest $request, Contact $contact)
    {
        $updatedContact = $request->validated();
        $contact = $this->contactService->hasUserContact(auth()->user()->id, $contact);
        $savedContact = $this->contactService->updateContact($contact->id ?? $contact, $updatedContact);
        return $savedContact;
    }
    public function destroy(Contact $contact)
    {
        //usar o repository
        $contactId = $contact->id;
        $contact = $this->contactService->hasUserContact(auth()->user()->id, $contact);
        if ($contact) {
            $this->contactService->deleteContact($contact->id);
            return [
                'status' => 'Contact deleted',
                'contact id' => $contactId,
            ];
        }
        return [
            'status' => '405 Method Not Allowed',
            'contact id' => $contactId
        ];
    }
}
