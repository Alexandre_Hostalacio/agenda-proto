<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $guarded = [];

    protected $casts = [
        'phones' => 'array',
        'addresses' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
